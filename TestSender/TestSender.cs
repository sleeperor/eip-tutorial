using Apache.NMS;
using System;
using System.Threading;
using log4net;
using TestModel;

namespace TestSender
{
	public class TestSender
	{
		private static readonly ILog LOG = LogManager.GetLogger (typeof(TestSender));

		IConnectionFactory ConnectionFactory { get; set; }

		String RequestDestinationName { get; set; }

		String ResponseDestinationName { get; set; }
		
		private IConnection connection;
		private ISession session;
		private IMessageProducer producer;
		private IDestination responseDestination;
		private Thread thread;
		private bool run = true;

		public TestSender (IConnectionFactory connectionFactory, String requestDestinationName, String responseDestinationName)
		{
			this.ConnectionFactory = connectionFactory;
			this.RequestDestinationName = requestDestinationName;
			this.ResponseDestinationName = responseDestinationName;
		}
		
		public void Start ()
		{
			this.connection = this.ConnectionFactory.CreateConnection ();
			this.session = connection.CreateSession ();
			this.producer = session.CreateProducer (session.GetDestination (RequestDestinationName));
			this.responseDestination = session.GetDestination (ResponseDestinationName);
			this.thread = new Thread (new ThreadStart (DoSend));
			this.thread.Start ();
		}
		
		public void Stop ()
		{
			this.run = false;
			this.thread.Join ();
			LOG.InfoFormat ("Stopped producer with connection [{0}]", this.connection.ClientId);
			
			if (this.producer != null) {
				this.producer.Dispose ();
			}
			if (this.session != null) {
				this.session.Dispose ();
			}
			if (this.connection != null) {
				this.connection.Dispose ();
			}
			
			LOG.Info ("Cleaned up resources");
		}

		public void DoSend ()
		{
			while (this.run) {
				var iTextMessage = producer.CreateXmlMessage (
					new InsuranceInquiry (
						new CarInfo ("Toyota", "Avensis", 2012, 2.0M, 5, "Hatchback", "XYZ-1234", new DateTime (2013, 01, 20), 120000), 
						new PersonalInfo ("John", "Doe", new DateTime (1980, 05, 23), "Warsaw", "01-321"), 
						new DateTime (2013, 01, 26)));
				iTextMessage.NMSReplyTo = responseDestination;
				
				LOG.InfoFormat ("Sending textMessage [{0}]", iTextMessage); 
				producer.Send (iTextMessage);
				
				Thread.Sleep (500);
			}
		}
	}
}

