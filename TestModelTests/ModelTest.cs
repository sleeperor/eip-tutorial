using NUnit.Framework;
using System;
using TestModel;

namespace TestModelTests
{
	[TestFixture]
	public class ModelTest
	{
		
		[Test]
		public void SomeTest ()
		{
			var inquiry = new InsuranceInquiry () {
				CarInfo = new CarInfo (),
				PersonalInfo = new PersonalInfo ()
			};
			
			Assert.IsNotNull (inquiry);
			Assert.IsNotNull (inquiry.CarInfo);
			Assert.IsNotNull (inquiry.PersonalInfo);
		}
	}
}

