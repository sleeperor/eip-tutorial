using Apache.NMS;
using System;
using System.Threading;
using log4net;
using TestModel;

namespace TestReceiver
{
	public class TestReceiver
	{
		private static readonly ILog LOG = LogManager.GetLogger (typeof(TestReceiver));
		private static int COUNTER = 0;

		IConnectionFactory connectionFactory { get; set; }

		String requestDestinationName { get; set; }

		String replyDestinationName { get; set; }

		private IConnection connection;
		private ISession session;
		private IMessageConsumer consumer;
				
		public TestReceiver (IConnectionFactory connectionFactory, string requestDestinationName)
		{
			this.connectionFactory = connectionFactory;
			this.requestDestinationName = requestDestinationName;
		}
		
		public void Start ()
		{
			this.connection = connectionFactory.CreateConnection ();
			this.connection.ClientId = "TestReceiver-" + ++COUNTER;
			
			this.session = connection.CreateSession ();
			
			this.consumer = session.CreateConsumer (session.GetDestination (requestDestinationName));
			this.consumer.Listener += OnMessage;
			
			this.connection.Start ();
			LOG.InfoFormat ("Started listener with connection [{0}]", this.connection.ClientId);
		}
		
		public void Stop ()
		{
			if (this.consumer != null) {
				this.consumer.Dispose ();
			}
			LOG.InfoFormat ("Stopped listener with connection [{0}]", this.connection.ClientId);
			if (this.connection != null) {
				this.connection.Dispose ();
			}
			if (this.session != null) {
				this.session.Dispose ();
			}
			LOG.Info ("Cleaned up resources");
		}

		public void OnMessage (IMessage message)
		{
			try {
				
				Console.WriteLine (message);
			
				using (IMessageProducer producer = session.CreateProducer(message.NMSReplyTo)) {
				
					var iTextMessage = producer.CreateXmlMessage (
					new InsuranceOffer (
						"offer-1-queue", new DateTime (2013, 01, 30), 
				    	new InsuranceVariant[]{
							new InsuranceVariant ("variant-1-queue", 3580, 3580, 1, "Single fee"),
							new InsuranceVariant ("variant-2-queue", 4800, 1200, 4, "Quarterly fee")}));
				
					iTextMessage.NMSCorrelationID = message.NMSCorrelationID;
				

					Thread.Sleep (800);			
					producer.Send (iTextMessage);
					LOG.InfoFormat ("Sending textMessage {0}, {1}, {2}", 
			                	iTextMessage.NMSMessageId, iTextMessage.NMSCorrelationID, iTextMessage.Text); 
				}
			
				//message.Acknowledge ();
			} catch (Exception e) {
				Console.WriteLine (e);
			}
		}
	}
}
