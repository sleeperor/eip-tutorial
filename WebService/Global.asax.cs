using System;
using System.Collections;
using System.ComponentModel;
using System.Web;
using System.Web.SessionState;
using log4net;
using log4net.Config;

namespace WebService
{
	public class Global : System.Web.HttpApplication
	{		
		private static readonly ILog log = LogManager.GetLogger (typeof(Global));
		
		protected virtual void Application_Start (Object sender, EventArgs e)
		{
			BasicConfigurator.Configure ();
			log.Info ("Application started");
		}
		
		protected virtual void Application_End (Object sender, EventArgs e)
		{
			log.Info ("Application stopped");
		}
	}
}

