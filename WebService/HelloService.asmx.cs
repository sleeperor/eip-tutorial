using System;
using System.Web;
using System.Web.Services;
using System.Xml.Serialization;
using log4net;

namespace WebService
{
	[WebService(Description="Query for insurance offer", 
	            Name="InsuranceInquiry",
	            Namespace="http://www.babymotors.com/2008/10/InsuranceInquiry")]
	[WebServiceBinding(ConformsTo=WsiProfiles.BasicProfile1_1)]
	public class InsuranceInquiryService : System.Web.Services.WebService
	{
		private static readonly ILog log = LogManager.GetLogger (typeof(InsuranceInquiryService));

		[WebMethod(Description="Query for insurance method")]
		public InsuranceOffer[] ApplyInquiry (PersonalInfo PersonalInfo, CarInfo CarInfo)
		{
			log.InfoFormat ("Hello({0}. {1}) invoked", PersonalInfo, CarInfo);
			return new InsuranceOffer[]{
				new InsuranceOffer (
						"offer-2", new DateTime (2013, 01, 30), 
				    	new InsuranceVariant[]{
							new InsuranceVariant ("variant-3", 3830, 3830, 1, "Single fee"),
							new InsuranceVariant ("variant-4", 4240, 1060, 4, "Quarterly fee")})
			};
			
		}
	}

	[XmlType(TypeName="PersonalInfoType")]
	public class PersonalInfo
	{
		[XmlElement(IsNullable=false)]
		public string FirstName{ get; set; }

		[XmlElement(IsNullable=false)]
		public string LastName{ get; set; }

		[XmlElement(IsNullable=false)]
		public string City{ get; set; }

		[XmlElement(IsNullable=false)]
		public string ZipCode{ get; set; }

		[XmlElement(IsNullable=false)]
		public string StreetName{ get; set; }

		[XmlElement(IsNullable=false)]
		public string StreetNumber{ get; set; }
	}

	[XmlType(TypeName="CarInfoType")]
	public class CarInfo
	{
		[XmlElement(IsNullable=false)]
		public DateTime ProductionDate{ get; set; }

		[XmlElement(IsNullable=false)]
		public string Brand{ get; set; }

		[XmlElement(IsNullable=false)]
		public string Make{ get; set; }

		[XmlElement(IsNullable=false)]
		public string VIN{ get; set; }
	}

	[XmlType(TypeName="InsuranceOfferType")]
	public class InsuranceOffer
	{
		[XmlElement(IsNullable=false)]
		public string ID { get; set; }
	
		[XmlElement(ElementName="OfferID", IsNullable=false)]
		public String OfferID { get; set; }
		
		[XmlElement(ElementName="ValidTo", IsNullable=false)]
		public DateTime ValidTo { get; set; }

		[XmlElement(ElementName="Variants", IsNullable=false)]
		public InsuranceVariant[] Variants { get; set; }
		
		public InsuranceOffer ()
		{
		}

		public InsuranceOffer (String offerID, DateTime validTo, InsuranceVariant[] variants)
		{
			this.OfferID = offerID;
			this.ValidTo = validTo;
			this.Variants = variants;
		}

		public override string ToString ()
		{
			return string.Format ("[InsuranceOffer: OfferID={0}, ValidTo={1}, Variants={2}]", OfferID, ValidTo, Variants);
		}
	}
	
	[XmlType(TypeName="InsuranceVariantType")]
		public class InsuranceVariant
	{
		
		public String VariantID { get; set; }

		public Decimal Total { get; set; }

		public Decimal Installment { get; set; }
		
		public int Installments { get; set; }
		
		public String Description { get; set; }
		
		public InsuranceVariant ()
		{
			
		}
		
		public InsuranceVariant (String variantID, Decimal total, Decimal installment, int installments, String description)
		{
			this.VariantID = variantID;
			this.Total = total;
			this.Installment = installment;
			this.Installments = installments;
			this.Description = description;
		}

		public override string ToString ()
		{
			return string.Format ("[InsuranceVariant: VariantID={0}, Total={1}, Installment={2}, Installments={3}, Description={4}]", 
			                      VariantID, Total, Installment, Installments, Description);
		}
	}
}

