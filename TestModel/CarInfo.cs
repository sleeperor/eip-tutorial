using System;
using System.Xml.Serialization;

namespace TestModel
{
	[XmlType(TypeName="CarInfoType",
	         Namespace="http://www.baby-motors.com/2007/04/InsuranceInquiry")]	
	public class CarInfo
	{
		public String Make { get; set; }
		
		public String Model { get; set; }
		
		public int  Year { get; set; }
		
		public Decimal EngineSize { get; set; }
		
		public int Seats { get; set; }

		public String Type { get; set; }
		
		public String RegistrationNumber { get; set; }

		public DateTime  PurchaseDate { get; set; }
		
		public Decimal EstimatedValue{ get; set; }
		
		public CarInfo ()
		{
			
		}
		
		public CarInfo (String make, String model, int year, Decimal engineSize, int seats, String type, String registrationNumber, DateTime purchaseDate, Decimal estimatedValue)
		{
			this.Make = make;
			this.Model = model;
			this.Year = year;
			this.EngineSize = engineSize;
			this.Seats = seats;
			this.Type = type;
			this.RegistrationNumber = registrationNumber;
			this.PurchaseDate = purchaseDate;
			this.EstimatedValue = estimatedValue;
		}

		public override string ToString ()
		{
			return string.Format ("[CarInfo: Make={0}, Model={1}, Year={2}, EngineSize={3}, Seats={4}, Type={5}, RegistrationNumber={6}, PurchaseDate={7}, EstimatedValue={8}]", 
			                      Make, Model, Year, EngineSize, Seats, Type, RegistrationNumber, PurchaseDate, EstimatedValue);
		}
	}
}

