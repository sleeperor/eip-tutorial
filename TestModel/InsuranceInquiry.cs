using System;
using System.Xml.Serialization;

namespace TestModel
{
	[XmlRoot(ElementName="InsuranceInquiryType",
	         Namespace="http://www.baby-motors.com/2007/04/InsuranceInquiry")]
	public class InsuranceInquiry
	{
		[XmlElement(ElementName="CarInfo", IsNullable=false)]
		public CarInfo CarInfo { get; set; }
		
		[XmlElement(ElementName="PersonalInfo", IsNullable=false)]
		public PersonalInfo PersonalInfo { get; set; }
		
		[XmlElement(ElementName="CoverFromDate", IsNullable=false)]
		public DateTime CoverFromDate { get; set; }
		
		public InsuranceInquiry ()
		{		
		}
		
		public InsuranceInquiry (CarInfo carInfo, PersonalInfo personalInfo, DateTime coverFromDate)
		{
			this.CarInfo = carInfo;
			this.PersonalInfo = personalInfo;
			this.CoverFromDate = coverFromDate;
		}
		
		public override string ToString ()
		{
			return string.Format ("[InsuranceInquiry: CarInfo={0}, PersonalInfo={1}, CoverFromDate={2}]", 
			                      CarInfo, PersonalInfo, CoverFromDate);
		}
	}
}

