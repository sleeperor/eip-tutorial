using System;
using System.Xml.Serialization;

namespace TestModel
{
	[XmlType(TypeName="PersonalInfoType",
	         Namespace="http://www.baby-motors.com/2007/04/InsuranceInquiry")]
	public class PersonalInfo
	{
		[XmlElement(IsNullable=false)]
		public String FirstName{ get; set; }

		[XmlElement(IsNullable=false)]
		public String LastName{ get; set; }

		[XmlElement(IsNullable=false)]
		public DateTime BirthDate{ get; set; }

		[XmlElement(IsNullable=false)]
		public String City{ get; set; }

		[XmlElement(IsNullable=false)]
		public String ZipCode{ get; set; }

		public PersonalInfo ()
		{
		}

		public PersonalInfo (String firstName, String lastName, DateTime birthDate, String city, String zipCode)
		{
			this.FirstName = firstName;
			this.LastName = lastName;
			this.BirthDate = birthDate;
			this.City = city;
			this.ZipCode = zipCode;
		}

		public override string ToString ()
		{
			return string.Format ("[PersonalInfo: FirstName={0}, LastName={1}, BirthDate={2}, City={3}, ZipCode={4}]", 
			                      FirstName, LastName, BirthDate, City, ZipCode);
		}
	}
}

