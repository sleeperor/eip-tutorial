using System;
using System.Xml.Serialization;

namespace TestModel
{
	[XmlRoot(ElementName="InsuranceOfferType",
	         Namespace="http://www.baby-motors.com/2007/04/InsuranceInquiry")]
	public class InsuranceOffer
	{
		[XmlElement(ElementName="OfferID", IsNullable=false)]
		public String OfferID { get; set; }
		
		[XmlElement(ElementName="ValidTo", IsNullable=false)]
		public DateTime ValidTo { get; set; }

		[XmlElement(ElementName="Variants", IsNullable=false)]
		public InsuranceVariant[] Variants { get; set; }
		
		public InsuranceOffer ()
		{
		}

		public InsuranceOffer (String offerID, DateTime validTo, InsuranceVariant[] variants)
		{
			this.OfferID = offerID;
			this.ValidTo = validTo;
			this.Variants = variants;
		}

		public override string ToString ()
		{
			return string.Format ("[InsuranceOffer: OfferID={0}, ValidTo={1}, Variants={2}]", OfferID, ValidTo, Variants);
		}
	}
}

