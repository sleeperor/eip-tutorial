using System;
using System.Xml.Serialization;

namespace TestModel
{
	[XmlType(TypeName="InsuranceVariantType",
	         Namespace="http://www.baby-motors.com/2007/04/InsuranceInquiry")]
	public class InsuranceVariant
	{
		
		public String VariantID { get; set; }

		public Decimal Total { get; set; }

		public Decimal Installment { get; set; }
		
		public int Installments { get; set; }
		
		public String Description { get; set; }
		
		public InsuranceVariant ()
		{
			
		}
		
		public InsuranceVariant (String variantID, Decimal total, Decimal installment, int installments, String description)
		{
			this.VariantID = variantID;
			this.Total = total;
			this.Installment = installment;
			this.Installments = installments;
			this.Description = description;
		}

		public override string ToString ()
		{
			return string.Format ("[InsuranceVariant: VariantID={0}, Total={1}, Installment={2}, Installments={3}, Description={4}]", 
			                      VariantID, Total, Installment, Installments, Description);
		}
	}
}

