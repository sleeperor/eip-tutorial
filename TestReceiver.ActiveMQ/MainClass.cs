using Apache.NMS.ActiveMQ;
using System;
using System.Configuration;
using System.Collections.Generic;
using System.Collections.Specialized;
using TestReceiver;
using log4net;
using log4net.Config;

namespace TestReceiver.ActiveMQ
{
	class MainClass
	{
		private static readonly ILog LOG = LogManager.GetLogger(typeof(TestReceiver));
		
		public static void Main (string[] args)
		{
			BasicConfigurator.Configure ();
			String brokerURL = ConfigurationManager.AppSettings ["brokerURL"];
			String requestDestinationName = ConfigurationManager.AppSettings ["requestDestinationName"];
			short threads = Convert.ToInt16 (ConfigurationManager.AppSettings ["threads"]);
				
			LOG.InfoFormat("Using brokerURL: {0}", brokerURL);
			LOG.InfoFormat("Using requestDestinationName: {0}", requestDestinationName);
			LOG.InfoFormat("Using threadCount: {0}", threads);
			
			var connectionFactory = new ConnectionFactory (brokerURL);
			var receivers = new List<TestReceiver> ();
			
			for (int ix = threads; ix > 0; ix--) {
				var receiver = new TestReceiver (connectionFactory, requestDestinationName);
				receiver.Start ();
				receivers.Add (receiver);
			}
			
			Console.ReadKey ();
			
			foreach (TestReceiver receiver in receivers) {
				receiver.Stop ();
			}			
		}
	}
}
