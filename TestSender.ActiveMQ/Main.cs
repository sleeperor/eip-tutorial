using Apache.NMS.ActiveMQ;
using System;
using System.Configuration;
using System.Collections.Specialized;
using TestSender;
using log4net;
using log4net.Config;

namespace TestSender.ActiveMQ
{
	class MainClass
	{
		private static readonly ILog LOG = LogManager.GetLogger(typeof(TestSender));
			
		public static void Main (string[] args)
		{
			BasicConfigurator.Configure ();
			String brokerURL = ConfigurationManager.AppSettings ["brokerURL"];
			String requestDestinationName = ConfigurationManager.AppSettings ["requestDestinationName"];
			String responseDestinationName = ConfigurationManager.AppSettings ["responseDestinationName"];
			
			LOG.InfoFormat("Using brokerURL: {0}", brokerURL);
			LOG.InfoFormat("Using requestDestinationName: {0}", requestDestinationName);
			LOG.InfoFormat("Using responseDestinationName: {0}", responseDestinationName);
			
			var connectionFactory = new ConnectionFactory (brokerURL);
			var sender = new TestSender (connectionFactory, requestDestinationName, responseDestinationName);
			sender.Start ();
			Console.ReadKey ();
			sender.Stop ();
		}
	}
}
